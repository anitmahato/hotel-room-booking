<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <title>Home Page</title>
</head>
<body>
    <h3>Add Rooms</h3>
     <form name="forms" id="forms" action="Room" method="post">
            <input type="text" name="roomNum"><br>
            <input type="text" name="hotelID"><br>
            <input type="text" name="roomType"><br>
            <input type="text" name="amount"><br>
            <input type="submit"><br>
     </form>

     <h3>Show Rooms</h3>
     <form action="Rooms">
            <input type="submit"><br>
     </form>

     <h3>Add Customer Details</h3>
          <form name="formx" id="formx" action="Customer" method="post">
                 <input type="text" name="customerID"><br>
                 <input type="text" name="name"><br>
                 <input type="text" name="aadharNumber"><br>
                 <input type="text" name="mobileNum"><br>
                 <input type="text" name="roomType"><br>
                 <input type="submit"><br>
          </form>
     <h3>Search Customer</h3>
           <form action="Customer">
                  <input type="text" name="CustID"><br>
                  <input type="submit"><br>
           </form>
     <h3>Checkout</h3>
            <form action="checkout" method="post">
                   <input type="text" name="CustID"><br>
                   <input type="submit"><br>
            </form>
     <script>
           document.addEventListener('DOMContentLoaded', () => {
             document
               .getElementById('forms')
               .addEventListener('submit', handleForm);
           });

           function handleForm(ev) {
             ev.preventDefault(); //stop the page reloading
             //console.dir(ev.target);
             let myForm = ev.target;
             let fd = new FormData(myForm);


             //look at all the contents
             for (let key of fd.keys()) {
               console.log(key, fd.get(key));
             }
             let json = convertFD2JSON(fd);

             //send the request with the formdata
             let url = 'http://localhost:8080/Room';
             let h = new Headers();
             h.append('Content-type', 'application/json');

             let req = new Request(url, {
               headers: h,
               body: json,
               method: 'POST',
             });
             //console.log(req);
             fetch(req)
               .then((res) => res.json())
               .then((data) => {
                 console.log('Response from server');
                 console.log(data);
               })
               .catch(console.warn);
           }

           function convertFD2JSON(formData) {
             let obj = {};
             for (let key of formData.keys()) {
               obj[key] = formData.get(key);
             }
             return JSON.stringify(obj);
           }
         </script>

</body>

</html>