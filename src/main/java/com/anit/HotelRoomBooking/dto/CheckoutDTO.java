package com.anit.HotelRoomBooking.dto;

public class CheckoutDTO {
    private int customerID;

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }
}
