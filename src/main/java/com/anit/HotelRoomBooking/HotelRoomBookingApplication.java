package com.anit.HotelRoomBooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.*")
public class HotelRoomBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelRoomBookingApplication.class, args);
	}

}
