package com.anit.HotelRoomBooking.models;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Entity;
import javax.persistence.Id;

public class Customer {
    private int customerID;
    private String name;
    private String aadharNumber;
    private String mobileNum;
    private CustomerStatus customerStatus;
    private int roomNum;


    public Customer() {
        this.customerStatus=CustomerStatus.CHECKEDIN;
    }

    public int getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(int roomNum) {
        this.roomNum = roomNum;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public CustomerStatus getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(CustomerStatus customerStatus) {
        this.customerStatus = customerStatus;
    }

    public void checkout()
    {
        this.customerStatus=CustomerStatus.CHECKEDOUT;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerID=" + customerID +
                ", name='" + name + '\'' +
                ", aadharNumber='" + aadharNumber + '\'' +
                ", mobileNum='" + mobileNum + '\'' +
                ", customerStatus=" + customerStatus +
                ", roomNum=" + roomNum +
                '}';
    }
}
