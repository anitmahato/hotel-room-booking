package com.anit.HotelRoomBooking.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;


public class Room {
    private int roomNum;
    private int hotelID;
    private RoomType roomType;
    private RoomStatus roomStatus;
    private int amount;


    public Room() {
        this.roomStatus=RoomStatus.UNBOOKED;
    }

    public int getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(int roomNum) {
        this.roomNum = roomNum;
    }

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNum=" + roomNum +
                ", hotelID=" + hotelID +
                ", roomType=" + roomType +
                ", roomStatus=" + roomStatus +
                ", amount=" + amount +
                '}';
    }

    public void bookRoom()
    {
        this.roomStatus=RoomStatus.BOOKED;
    }
    public void checkoutRoom()
    {
        this.roomStatus=RoomStatus.UNBOOKED;
    }

}
