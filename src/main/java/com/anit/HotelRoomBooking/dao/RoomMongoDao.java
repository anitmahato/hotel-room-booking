package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.models.Room;
import com.anit.HotelRoomBooking.models.RoomStatus;
import com.anit.HotelRoomBooking.models.RoomType;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoWriteException;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.logging.Logger;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
@Component
public class RoomMongoDao {

    private MongoClient mongoClient;

    private MongoDatabase database;

//    private String uri = "mongodb+srv://anitmahato:anit123@hotelroombooking.5k3tk.mongodb.net/admin";

    private Document document;

//    private Bson bson;

    private final MongoCollection<Room> roomCollection;

    public RoomMongoDao(@Value("${spring.mongodb.uri}") String uri) {
        mongoClient = MongoClients.create(uri);
        database = mongoClient.getDatabase("Hotel");
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        roomCollection = database.getCollection("Room",Room.class).withCodecRegistry(pojoCodecRegistry);

    }
    public ArrayList<Room> getRoom(){
        ArrayList<Room> roomArrayList = new ArrayList<>();
        roomCollection.find().sort(Sorts.ascending("roomNum")).into(roomArrayList);
        return roomArrayList;
    }
    public Room getRoom(int roomnum){
        return roomCollection.find(new Document("roomNum",roomnum)).first();
    }

    public boolean addRoom(Room room){
        try {
            if(roomCollection.find(new Document("roomNum",room.getRoomNum())).iterator().tryNext()==null) {
                roomCollection.insertOne(room);
                return true;
            }
            return false;
        }
        catch (MongoWriteException e){
            System.out.println(e);
        }
        return false;
    }

    public int getRoomByType(RoomType roomType){
        String st;
        if(roomType == RoomType.SINGLE) {
            st = "SINGLE";
        }
        else if (roomType == RoomType.DOUBLE) {
            st = "DOUBLE";
        }
        else {
            st = "TRIPLE";
        }
        Room room;
        Bson filter = Filters.and(Filters.eq("roomType",st),Filters.eq("roomStatus", "UNBOOKED"));
        if(roomCollection.find(filter).iterator().tryNext()!=null){
            room = roomCollection.find(filter).first();
            roomCollection.deleteOne(filter);
            room.bookRoom();
            roomCollection.insertOne(room);
            return room.getRoomNum();
        }
        else {
            return 0;
        }
    }
    public void CheckoutRoom(int roomnum){
        Bson filter = Filters.eq("roomNum",roomnum);
        Room room = roomCollection.find(filter).first();
        roomCollection.deleteOne(filter);
        room.checkoutRoom();
        roomCollection.insertOne(room);
    }

    public void DeleteRoom(int roomnum){
        Bson filter = Filters.eq("roomNum",roomnum);
        roomCollection.deleteOne(filter);
    }

}
