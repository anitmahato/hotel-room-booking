package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.models.*;
import org.springframework.stereotype.Component;

import java.sql.*;
@Component
public class CustomerDao {
    public Connection connect() throws Exception
    {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/hoteldb", "theviperanit", "ankit123");
        return connection;
    }
    public Customer getCustomer(int custID) throws Exception {
        String query = "select * from Customers where custid="+custID;
        Customer customer = new Customer();
        customer.setCustomerID(custID);
        Connection connection= connect();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        rs.next();
        customer.setName(rs.getString(2));
        customer.setAadharNumber(rs.getString(3));
        customer.setMobileNum(rs.getString(4));
        customer.setCustomerStatus(CustomerStatus.valueOf(rs.getString(5)));
        customer.setRoomNum(Integer.valueOf(rs.getInt(6)));
        statement.close();
        connection.close();
        return customer;
    }
    public boolean addCustomer(Customer customer,RoomType roomType) throws Exception{
        String query1 = "insert into Customers values (?,?,?,?,?,?);";
        String query2 = "select * from Rooms where roomtype='"+roomType +"' and roomstatus='UNBOOKED'";
        Connection connection= connect();
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement();
        ResultSet rs = statement.executeQuery(query2);
        if (rs.next()) {
            PreparedStatement statement1 = connection.prepareStatement(query1);
            statement1.setInt(1, customer.getCustomerID());
            statement1.setString(2, customer.getName());
            statement1.setString(3, customer.getAadharNumber());
            statement1.setString(4, customer.getMobileNum());
            statement1.setString(5, "CHECKEDIN");
            statement1.setInt(6, rs.getInt(1));
            String query3 = "update Rooms set roomstatus='BOOKED' where roomnum="+ rs.getInt(1);
            statement2.executeUpdate(query3);
            statement1.executeUpdate();
            statement.close();
            statement1.close();
            connection.close();
            return true;
        }
        statement.close();
        connection.close();
        return false;
    }
}
