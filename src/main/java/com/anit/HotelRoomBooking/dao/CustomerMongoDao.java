package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.models.Customer;
import com.anit.HotelRoomBooking.models.CustomerStatus;
import com.anit.HotelRoomBooking.models.Room;
import com.anit.HotelRoomBooking.models.RoomType;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
@Component
public class CustomerMongoDao {
    private MongoClient mongoClient;

    private MongoDatabase database;

    private Document document;

    private Bson bson;

    private final MongoCollection<Customer> customerCollection;

    @Autowired
    RoomMongoDao roomDao;

    public CustomerMongoDao(@Value("${spring.mongodb.uri}") String uri)
    {
        mongoClient = MongoClients.create(uri);
        database = mongoClient.getDatabase("Hotel");
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        customerCollection = database.getCollection("Customer", Customer.class).withCodecRegistry(pojoCodecRegistry);

    }
    public boolean addCustomer(Customer customer, RoomType roomType){
        try {
            Bson filter = Filters.eq("customerID",customer.getCustomerID());
            if(customerCollection.find(filter).iterator().tryNext()==null) {
                int roomNum =roomDao.getRoomByType(roomType);
                if(roomNum!=0) {
                    customer.setRoomNum(roomNum);
                    customerCollection.insertOne(customer);
                    return true;
                }
                return false;

            }
            return false;
        }
        catch (MongoWriteException e){
            System.out.println(e);
        }
        return false;
    }
    public Boolean checkCustomer(int custID){
        Bson filter = Filters.eq("customerID",custID);
        if(customerCollection.find(filter).iterator().tryNext()==null){
            return true;
        }
        return false;
    }
    public Customer getCustomer(int custID){
        Bson filter = Filters.eq("customerID",custID);
        return customerCollection.find(filter).first();
    }
    public ArrayList<Customer> getCustomer(){
        ArrayList<Customer> customers = new ArrayList<>();
        customerCollection.find().sort(Sorts.ascending("customerID")).into(customers);
        return customers;
    }
    public void Checkout(int custID){
        Bson filter = Filters.eq("customerID",custID);
        Customer customer = customerCollection.find(filter).first();
        customerCollection.deleteOne(filter);
        customer.setCustomerStatus(CustomerStatus.CHECKEDOUT);
        customerCollection.insertOne(customer);
        roomDao.CheckoutRoom(customer.getRoomNum());
    }
}
