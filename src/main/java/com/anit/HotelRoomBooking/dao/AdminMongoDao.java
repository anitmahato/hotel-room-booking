package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.models.Admin;
import com.anit.HotelRoomBooking.models.Room;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Component
public class AdminMongoDao {
    private MongoClient mongoClient;

    private MongoDatabase database;

//    private String uri = "mongodb+srv://anitmahato:anit123@hotelroombooking.5k3tk.mongodb.net/admin";

    private Document document;

    private Bson bson;

    private final MongoCollection<Admin> adminCollection;

    public AdminMongoDao(@Value("${spring.mongodb.uri}") String uri)
    {
        mongoClient = MongoClients.create(uri);
        database = mongoClient.getDatabase("Hotel");
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        adminCollection = database.getCollection("Admin",Admin.class).withCodecRegistry(pojoCodecRegistry);

    }
    public ArrayList<Admin> getAdmin(){
        ArrayList<Admin> adminArrayList = new ArrayList<>();
        adminCollection.find().into(adminArrayList);
        return adminArrayList;
    }
}
