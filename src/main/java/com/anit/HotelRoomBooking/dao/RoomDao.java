package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.models.CustomerStatus;
import com.anit.HotelRoomBooking.models.Room;
import com.anit.HotelRoomBooking.models.RoomStatus;
import com.anit.HotelRoomBooking.models.RoomType;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
@Component
public class RoomDao {
    public Connection connect() throws Exception
    {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/hoteldb", "theviperanit", "ankit123");
        return connection;
    }
    public ArrayList<Room> getRoom() throws Exception {
        ArrayList<Room> roomList = new ArrayList<>();
        String query = "select * from Rooms;";
        Connection connection = connect();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        while (rs.next()) {
            Room room = new Room();
            room.setRoomNum(rs.getInt(1));
            room.setHotelID(rs.getInt(2));
            room.setRoomType(RoomType.valueOf(rs.getString(3)));
            room.setRoomStatus(RoomStatus.valueOf(rs.getString(4)));
            room.setAmount(Integer.valueOf(rs.getInt(5)));
            roomList.add(room);
        }
        statement.close();
        connection.close();
        return roomList;
    }
    public Room getRoom(int roomNum) throws Exception {
        String query = "select * from Rooms where roomnum="+roomNum;
        Room room = new Room();
        room.setRoomNum(roomNum);
        Connection connection= connect();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        rs.next();
        room.setHotelID(rs.getInt(2));
        room.setRoomType(RoomType.valueOf(rs.getString(3)));
        room.setRoomStatus(RoomStatus.valueOf(rs.getString(4)));
        room.setAmount(Integer.valueOf(rs.getInt(5)));
        statement.close();
        connection.close();
        return room;
    }
    public void addRoom(Room room) throws Exception
    {
        String query = "insert into Rooms values (?,?,?,?,?);";
        Connection connection= connect();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1,room.getRoomNum());
        statement.setInt(2,room.getHotelID());
        statement.setString(3,String.valueOf(room.getRoomType()));
        statement.setString(4,String.valueOf(room.getRoomStatus()));
        statement.setInt(5,room.getAmount());
        statement.executeUpdate();
        statement.close();
        connection.close();
    }
    public void Checkout(int custid) throws Exception{
        Connection connection = connect();
        String query1 = "select * from Customers where custid="+custid;
        String query2 = "update Customers set customerstatus='"+ CustomerStatus.CHECKEDOUT+"' where custid="+custid;
        Statement statement1= connection.createStatement();
        ResultSet rs = statement1.executeQuery(query1);
        rs.next();
        String query3 = "update Rooms set roomstatus='UNBOOKED' where roomnum="+ rs.getInt(6);
        Statement statement2 = connection.createStatement();
        statement2.executeUpdate(query3);
        Statement statement3 = connection.createStatement();
        statement3.executeUpdate(query2);
    }
}
