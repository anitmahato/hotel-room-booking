package com.anit.HotelRoomBooking.controllers;

import com.anit.HotelRoomBooking.dao.AdminMongoDao;
import com.anit.HotelRoomBooking.models.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class AdminController {
    @Autowired
    AdminMongoDao adminMongoDao;
    @GetMapping("/admin")
    public ArrayList<Admin> getAdmin(){
        return adminMongoDao.getAdmin();
    }
}
