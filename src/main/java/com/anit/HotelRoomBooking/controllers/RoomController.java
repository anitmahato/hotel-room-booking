package com.anit.HotelRoomBooking.controllers;
import com.anit.HotelRoomBooking.dao.*;
import com.anit.HotelRoomBooking.dto.CustomerDto;
import com.anit.HotelRoomBooking.dto.RoomDto;
import com.anit.HotelRoomBooking.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
public class RoomController {
    @Autowired
    RoomDao dao;
    @Autowired
    CustomerDao cdao;
    @Autowired
    RoomMongoDao roomDaoMongo;
    @Autowired
    CustomerMongoDao customerMongoDao;
    @Autowired
    AdminMongoDao adminMongoDao;
    @PostMapping("/Room")
    public void addRoom(@RequestBody RoomDto roomDto) throws Exception {
        Room room = new Room();
        room.setRoomNum(roomDto.getRoomNum());
        room.setHotelID(roomDto.getHotelID());
        room.setRoomType(roomDto.getRoomType());
        room.setAmount(roomDto.getAmount());
//        dao.addRoom(room);

        roomDaoMongo.addRoom(room);
    }
    @GetMapping("/Rooms")
    @ResponseBody
    public List<Room> showRoom() throws Exception {
//        return dao.getRoom();
        return roomDaoMongo.getRoom();
    }

    @GetMapping("/Room/{id}")
    @ResponseBody
    public Room showRoom(@PathVariable("id") int id) throws Exception {
//        return dao.getRoom(id);
        return roomDaoMongo.getRoom(id);
    }
    @PostMapping("/deleteRoom")
    public void deleteRoom(@RequestBody RoomDto roomDto) throws Exception{
        roomDaoMongo.DeleteRoom(roomDto.getRoomNum());
    }

}
