package com.anit.HotelRoomBooking.controllers;

import com.anit.HotelRoomBooking.dao.CustomerMongoDao;
import com.anit.HotelRoomBooking.dto.CustomerDto;
import com.anit.HotelRoomBooking.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Random;

@RestController
public class CustomerController {
    @Autowired
    CustomerMongoDao customerMongoDao;

    @PostMapping("/Customer")
    public Customer addCustomer(@RequestBody CustomerDto customerDto) throws Exception {
        Customer cust = new Customer();
        Random random = new Random();
        Boolean flags;
        int custid;
        while (true) {
            custid = random.nextInt(1000);
            flags = customerMongoDao.checkCustomer(custid);
            if(flags == true)
                break;
        }
        cust.setCustomerID(custid);
//        cust.setCustomerID(customerDto.getCustomerID());
        cust.setName(customerDto.getName());
        cust.setAadharNumber(customerDto.getAadharNumber());
        cust.setMobileNum(customerDto.getMobileNum());
//        Boolean b = cdao.addCustomer(cust,customerDto.getRoomType());
        Boolean c = customerMongoDao.addCustomer(cust,customerDto.getRoomType());
        if(!c)
            System.out.println("Sorry, no " + customerDto.getRoomType() + " rooms are available");
        return customerMongoDao.getCustomer(custid);
    }
    @PostMapping("/customer")
    public Customer addcustomer(CustomerDto customerDto) throws Exception {
        Customer cust = new Customer();
        Random random = new Random();
        Boolean flags;
        int custid;
        while (true) {
            custid = random.nextInt(1000);
            flags = customerMongoDao.checkCustomer(custid);
            if(flags == true)
                break;
        }
        cust.setCustomerID(custid);
//        cust.setCustomerID(customerDto.getCustomerID());
        cust.setName(customerDto.getName());
        cust.setAadharNumber(customerDto.getAadharNumber());
        cust.setMobileNum(customerDto.getMobileNum());
//        Boolean b = cdao.addCustomer(cust,customerDto.getRoomType());
        Boolean c = customerMongoDao.addCustomer(cust,customerDto.getRoomType());
        if(!c)
            System.out.println("Sorry, no " + customerDto.getRoomType() + " rooms are available");
        return customerMongoDao.getCustomer(custid);
    }
    @GetMapping("/Customers")
    @ResponseBody
    public ArrayList<Customer> searchCustomers() throws Exception {
//        return cdao.getCustomer(CustID);
        return customerMongoDao.getCustomer();
    }
    @GetMapping("/Customer/{id}")
    @ResponseBody
    public Customer searchCustomer(@PathVariable("id") int CustID) throws Exception {
//        return cdao.getCustomer(CustID);
        return customerMongoDao.getCustomer(CustID);
    }
    @PostMapping("/checkout")
    public void checkout(@RequestBody CustomerDto customerDto) throws Exception {
//        dao.Checkout(customerDto.getCustomerID);
        customerMongoDao.Checkout(customerDto.getCustomerID());
    }
}
