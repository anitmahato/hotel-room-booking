package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.HotelRoomBookingApplication;
import com.anit.HotelRoomBooking.models.Customer;
import com.anit.HotelRoomBooking.models.Room;
import com.anit.HotelRoomBooking.models.RoomStatus;
import com.anit.HotelRoomBooking.models.RoomType;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HotelRoomBookingApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RoomMongoDaoTest {

    private MongoClient mongoClient;

    private MongoDatabase database;

    private Document document;

    private Bson bson;

    @Value("${spring.mongodb.uri}")
    private String uri;

    private MongoCollection<Room> roomCollection;

    @Autowired
    RoomMongoDao roomMongoDao;

    public void RoomMongoDao() {
        mongoClient = MongoClients.create(uri);
        database = mongoClient.getDatabase("Hotel");
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        roomCollection = database.getCollection("Room", Room.class).withCodecRegistry(pojoCodecRegistry);

    }

    @Test
    void getRoom() {
        RoomMongoDao();
        ArrayList<Room> list1 = new ArrayList<>();
        roomCollection.find().into(list1);
        ArrayList<Room> list = roomMongoDao.getRoom();
        Assert.assertEquals(list1.size(), list.size());
    }

    @Test
    void testGetRoom() {
        RoomMongoDao();
        Room room = roomMongoDao.getRoom(405);
        Assert.assertEquals(RoomType.TRIPLE, room.getRoomType());
    }

    @Test
    void addRoom() {
        RoomMongoDao();
        Room room = new Room();
        room.setRoomNum(55);
        room.setRoomStatus(RoomStatus.BOOKED);
        ArrayList<Room> list = new ArrayList<>();
        roomCollection.find().into(list);
        int num = list.size();
        roomMongoDao.addRoom(room);
        ArrayList<Room> list1 = new ArrayList<>();
        roomCollection.find().into(list1);
        int num1 = list1.size();

        Bson filter = Filters.eq("roomNum", 55);
        Room room1 = roomCollection.find(filter).first();
        Assert.assertEquals(num + 1, num1);
        Assert.assertEquals(room.getRoomStatus(), room1.getRoomStatus());

    }

    @Test
    void getRoomByType() {
        RoomMongoDao();
        Room room = new Room();
        room.setRoomNum(8888);
        room.setRoomType(RoomType.TRIPLE);
        room.setRoomStatus(RoomStatus.UNBOOKED);
        roomCollection.insertOne(room);
        int roomNum = roomMongoDao.getRoomByType(RoomType.TRIPLE);
        System.out.println(roomNum);
        Bson filter = Filters.eq("roomNum", roomNum);
        Room room1 = roomCollection.find(filter).first();
        Assert.assertEquals(RoomType.TRIPLE, room1.getRoomType());
        Assert.assertEquals(RoomStatus.BOOKED, room1.getRoomStatus());
    }

    @Test
    void checkoutRoom() {
        RoomMongoDao();

        Room room2 = new Room();
        room2.setRoomNum(4444);
        room2.setRoomStatus(RoomStatus.BOOKED);
        roomCollection.insertOne(room2);

        roomMongoDao.CheckoutRoom(4444);
        Bson filter1 = Filters.eq("roomNum", 4444);
        Room room1 = roomCollection.find(filter1).first();
        Assert.assertEquals(RoomStatus.UNBOOKED, room1.getRoomStatus());
    }

    @Test
    void deleteRoom() {
        RoomMongoDao();
        Room room = new Room();
        room.setRoomNum(9999);
        roomCollection.insertOne(room);
        ArrayList<Room> list = new ArrayList<>();
        roomCollection.find().into(list);
        int num = list.size();
        roomMongoDao.DeleteRoom(9999);
        ArrayList<Room> list1 = new ArrayList<>();
        roomCollection.find().into(list1);
        int num1 = list1.size();
        Assert.assertEquals(num - 1, num1);


        Bson filter = Filters.eq("rooNum", 9999);
        Room room1 = roomCollection.find(filter).first();
        Assert.assertEquals(null, room1);
    }
}