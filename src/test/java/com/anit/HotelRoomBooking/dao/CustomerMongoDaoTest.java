package com.anit.HotelRoomBooking.dao;

import com.anit.HotelRoomBooking.HotelRoomBookingApplication;
import com.anit.HotelRoomBooking.models.*;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HotelRoomBookingApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerMongoDaoTest {

    @Autowired
    CustomerMongoDao customerMongoDao;
    private MongoClient mongoClient;

    private MongoDatabase database;

    private Document document;

    private Bson bson;

    @Value("${spring.mongodb.uri}")
    private String uri;

    private MongoCollection<Customer> customerCollection;
    private MongoCollection<Room> roomCollection;

    @Before
    public void CustomerMongoDao()
    {
        this.mongoClient = MongoClients.create(uri);
        this.database = mongoClient.getDatabase("Hotel");
        CodecRegistry pojoCodecRegistry =
                fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        this.customerCollection = database.getCollection("Customer", Customer.class).withCodecRegistry(pojoCodecRegistry);
        this.roomCollection = database.getCollection("Room",Room.class).withCodecRegistry(pojoCodecRegistry);
    }
    @Test
    @Order(1)
    void addCustomer() {
        CustomerMongoDao();
        Customer customer = new Customer();
        customer.setCustomerID(123);
        customer.setName("Phil Foden");
        ArrayList<Customer> list = new ArrayList<>();
        ArrayList<Customer> list1 = new ArrayList<>();
        customerCollection.find().into(list);
        int num=list.size();

        Room room = new Room();
        room.setRoomNum(7777);
        room.setRoomType(RoomType.TRIPLE);
        room.setRoomStatus(RoomStatus.UNBOOKED);
        roomCollection.insertOne(room);


        customerMongoDao.addCustomer(customer, RoomType.TRIPLE);
        customerCollection.find().into(list1);
        int num1=list1.size();
        Bson filter = Filters.eq("customerID",123);
        Customer customer1 = customerCollection.find(filter).first();
        Assert.assertEquals(num+1,num1);
        Assert.assertEquals("Phil Foden",customer1.getName());
    }

    @Test
    @Order(2)
    void checkCustomer() {
        CustomerMongoDao();

        Customer customer = new Customer();
        customer.setCustomerID(1234);
        customerCollection.insertOne(customer);

        Boolean b=customerMongoDao.checkCustomer(1234);
        Assert.assertEquals(false,b);
    }

    @Test
    @Order(3)
    void getCustomer() {
        CustomerMongoDao();
        ArrayList<Customer> customers = customerMongoDao.getCustomer();
        ArrayList<Customer> customers1 = new ArrayList<>();
        customerCollection.find().into(customers1);
        Assert.assertEquals(customers1.size(),customers.size());
    }

    @Test
    @Order(4)
    void testGetCustomer() {
        CustomerMongoDao();
        Customer customer1 = customerCollection.find().first();
        Customer customer = customerMongoDao.getCustomer(customer1.getCustomerID());
        Assert.assertEquals(customer1.getName(),customer.getName());
    }

    @Test
    @Order(5)
    void checkout() {
        CustomerMongoDao();

        Room room = new Room();
        room.setRoomNum(55555);
        room.setRoomStatus(RoomStatus.BOOKED);
        roomCollection.insertOne(room);

        Customer customer2 =new Customer();
        customer2.setCustomerID(5555);
        customer2.setCustomerStatus(CustomerStatus.CHECKEDIN);
        customer2.setRoomNum(55555);
        customerCollection.insertOne(customer2);


        Bson filter = Filters.eq("customerID",5555);
        Customer customer = customerCollection.find(filter).first();
        customerMongoDao.Checkout(customer.getCustomerID());
        Bson filter1 = Filters.eq("customerID",customer.getCustomerID());
        Customer customer1 = customerCollection.find(filter1).first();
        Assert.assertEquals(CustomerStatus.CHECKEDOUT,customer1.getCustomerStatus());
    }
}